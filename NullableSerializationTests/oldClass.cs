﻿namespace NullableSerializationTests;

public class oldClass
{
    public string field1 { get; set; }
    public string field2 { get; set; }

    public static oldClass createOldClass()
    {
        oldClass c = new oldClass();
        c.field1 = "val1";
        c.field2 = "val2";

        oldClass x = new oldClass { field1 = "val1", field2 = "val2" };
        
        return c;
    }
}