﻿namespace NullableSerializationTests;

public class notnullableclass
{
    public notnullableclass(string notnull1, string notnull2)
    {
        this.notnull1 = notnull2;//специально так, чтобы запутать десериализатор
        this.notnull2 = notnull1;
    }
    public string notnull1 { get; set; }
    public string notnull2 { get; set; }
}