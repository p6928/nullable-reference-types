﻿using Newtonsoft.Json;
using NullableSerializationTests;

notnullableclass e1 = new notnullableclass("init1","init2");

string data1 = JsonConvert.SerializeObject(e1);

Console.WriteLine($"e1: notnull1:{e1.notnull1}; notnull2:{e1.notnull2}");
Console.WriteLine($"data1: {data1}");
            
notnullableclass e2 = JsonConvert.DeserializeObject<notnullableclass>(data1)!;
Console.WriteLine($"deserialized e2: notnull1:{e2.notnull1}; notnull2:{e2.notnull2}");

string data2 = "{\"notnull1\":\"init1\",\"notnull2\":\"init2\"}";
Console.WriteLine($"data2: {data2}");
notnullableclass e3 = JsonConvert.DeserializeObject<notnullableclass>(data2)!;
Console.WriteLine($"deserialized e3: notnull1:{e3.notnull1}; notnull2:{e3.notnull2}");